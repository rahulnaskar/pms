package com.rahul.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.rahul.client.service.LaunchControl;
import com.rahul.client.service.UserManagement;
import com.rahul.client.service.UserManagementAsync;
import com.rahul.model.MUser;
import com.rahul.shared.FieldVerifier;

public class AddUserWidget extends DialogBox implements AsyncCallback, UserManagementAsync {
	
	private UserManagementAsync service;
	String url;
	AsyncCallback callBack;
	MUser mUser;
	Login mLogin;

	private static AddUserWidgetUiBinder uiBinder = GWT
			.create(AddUserWidgetUiBinder.class);

	interface AddUserWidgetUiBinder extends UiBinder<Widget, AddUserWidget> {
	}

	public AddUserWidget() {
		init();
	}
	public AddUserWidget(Login pLogin) {
		init();
		mLogin = pLogin;
	}
	private void init()
	{
		setWidget(uiBinder.createAndBindUi(this));
		url = GWT.getModuleBaseURL() + "usermanagementservice";
		
		this.service = GWT.create(UserManagement.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);

		
		mUser = new MUser();
		
		this.setAnimationEnabled(true);
		this.center();
		this.show();
	}
	@UiField Button cmdSave;
	@UiField Button cmdCancel;
	@UiField TextBox txt_user_id;
	@UiField TextBox txt_user_name;
	@UiField TextBox txt_user_pwd1;
	@UiField TextBox txt_user_pwd2;
	@UiField Label lbluser_id;
	@UiField Label lbluser_name;
	@UiField Label lbluser_pwd1;
	@UiField Label lbluser_pwd2;
	@UiField HTML lblResult;
	@UiField HTMLPanel resultPanel;
	


	@UiHandler("cmdSave")
	void onClickSave(ClickEvent e) {
		boolean retVal = true;
		this.lblResult.setText("");
		String errMsg = "";
		if (!FieldVerifier.isValidLength(this.txt_user_pwd1.getText().trim(),6))
		{
			
			errMsg += "Give something bigger than 5 in your Password";
			retVal = false;
		}

		if (!FieldVerifier.isStringsSame(this.txt_user_pwd1.getText().trim(),this.txt_user_pwd2.getText().trim()))
		{
			if (errMsg.trim().length() > 0)
				errMsg += "<br>+";
			errMsg += "Your passwords don't match, sweetheart!";
			retVal = false;
		}
		if (!retVal)
		{
			this.lblResult = new HTML(errMsg);
			this.resultPanel.add(lblResult);
			return;
		}
		mUser.setUser_id(this.txt_user_id.getText().trim());
		mUser.setUser_name(this.txt_user_name.getText().trim());
		mUser.setUser_pwd(this.txt_user_pwd1.getText().trim());
		addUser(mUser,AddUserWidget.this);
	}
	@UiHandler("cmdCancel")
	void onClickCancel(ClickEvent e) {
		this.hide();
	}
	public void setText(String text) {
		cmdSave.setText(text);
	}

	public String getText() {
		return cmdSave.getText();
	}
	@Override
	public void onFailure(Throwable caught) {
		this.lblResult.setText("Oops...Something went horribly wrong!!!");
	}
	@Override
	public void onSuccess(Object result) {
		if (result instanceof Boolean)
		{
			if ((Boolean) result == true)
			{
				System.out.println("I just added your credentials...");
				this.lblResult.setText("I just added your credentials...");
				this.hide();
				mLogin.doLogin(this.txt_user_id.getText(), this.txt_user_pwd1.getText(), mLogin);
			}
			else
			{
				this.lblResult.setText("Oops...Your credentials didn't match!!!");
			}
		}		
		
	}
	@Override
	public void addUser(MUser mUser, AsyncCallback<Boolean> callback) {
		this.service.addUser(this.mUser, callback);
	}

}
