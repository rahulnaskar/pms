package com.rahul.server.dbqueries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Mqueries {
	private Connection dbCon;
	private Statement statement = null;
	private ResultSet resultSet = null;
	  
	public Mqueries(Connection pCon)
	{
		dbCon = pCon;
	}
	public static boolean insData(Connection pCon, String pQuery) throws SQLException
	{
		boolean retVal=false;
		Statement statement = pCon.createStatement();
		int resultSet = statement.executeUpdate(pQuery);
		if (resultSet > 0)
			retVal = true;
		return retVal;
	}
	public ResultSet getResult(String pQuery) throws SQLException
	{
		
		statement = dbCon.createStatement();
		resultSet = statement.executeQuery(pQuery);
		return resultSet;
	}
	public static ResultSet getResult(Connection pCon, String pQuery) throws SQLException
	{
		
		Statement statement = pCon.createStatement();
		ResultSet resultSet = statement.executeQuery(pQuery);
		return resultSet;
	}
}
