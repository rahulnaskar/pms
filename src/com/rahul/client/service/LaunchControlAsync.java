package com.rahul.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.rahul.model.MUser;


public interface LaunchControlAsync {
	void doLogin(String username, String password, AsyncCallback callback);
	void addTwoNumber(int num1, int num2, AsyncCallback callback);
	void getUserName(AsyncCallback callback);
	void getUserID(AsyncCallback callback);
}
