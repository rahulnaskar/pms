package com.rahul.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.rahul.model.MGroup;

public interface GroupManagementAsync {

	void getGroups(AsyncCallback callback);

}
