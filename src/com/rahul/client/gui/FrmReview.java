package com.rahul.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.rahul.client.service.DbCommService;
import com.rahul.client.service.DbCommServiceAsync;
import com.rahul.model.Reviews;
import com.rahul.shared.FieldVerifier;

import java.sql.Date;

public class FrmReview extends Composite implements HasText {

	private static FrmReviewUiBinder uiBinder = GWT.create(FrmReviewUiBinder.class);
	private  DbCommServiceAsync dbService=null;
	private Reviews revData;
	String url;
	int PlanID; //The Plan for which review is being done
	int ReviewID; //If an existing record is requested, the caller should pass the ID...stored here.
	int Mode; //To tag whether it's in Editing/Adding Mode. To be used to load data for the existing records, nothing otherwise. Would have liked enum, but kind of handicapped with Java on this...Java Sucks!!!
	
	interface FrmReviewUiBinder extends UiBinder<Widget, FrmReview> {
	}

	@UiField Label lstPlans;
	@UiField DateBox dpProj_start;
	@UiField TextBox txtproj_bout_item;
	@UiField TextBox txtproj_outs_cost;
	@UiField TextBox txtproj_direct_cost;
	@UiField TextBox txtproj_other_cost;
	@UiField TextBox txtproj_resources;
	@UiField TextBox txtproj_days;
	@UiField TextBox txtproj_items_ok;
	@UiField TextBox txtproj_items_not_ok;
	@UiField TextBox txtproj_items_missing;
	@UiField Label lblError;
	
	
	//Constructor for Add Mode
	public FrmReview(int pPlanId) {
		//Call the fx to create the service references and like...Removes duplication for Add/Edit
		initServiceDefs(pPlanId);
		Mode = 0;
		revData = new Reviews();
	}
	//Constructor for Edit Mode
	public FrmReview(int pPlanId, int pReviewId)
	{
		//Call the fx to create the service references and like...Removes duplication for Add/Edit
		initServiceDefs(pPlanId);
		ReviewID = pReviewId;
		initReviews(pPlanId, pReviewId);
		Mode = 0; //Editing
		
	}
	//Load Service references and set the Plan ID for which the review is being done
	void initServiceDefs(int pPlanId)
	{
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		PlanID = pPlanId;
		dpProj_start.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
		//Load nothing if it's not in editing mode.
	}
	//Load Data if it's in editing mode
	void initReviews(int pPlanId, int pReviewId)
	{
		AsyncCallback<Reviews> callBackAsync=new AsyncCallback<Reviews>() 
		{
			
			@Override
			public void onSuccess(Reviews result) 
			{
				dpProj_start.setValue(result.getStart_date());
				txtproj_outs_cost.setText(String.valueOf(result.getOutsourcing_cost()));
				txtproj_bout_item.setText(String.valueOf(result.getBought_out_cost()));
				txtproj_direct_cost.setText(String.valueOf(result.getDirect_charge()));
				txtproj_other_cost.setText(String.valueOf(result.getOther_cost()));
				txtproj_resources.setText(String.valueOf(result.getNo_of_resources()));
				txtproj_days.setText(String.valueOf(result.getDuration()));
				txtproj_items_ok.setText(String.valueOf(result.getItem_ok()));
				txtproj_items_not_ok.setText(String.valueOf(result.getItem_not_ok()));
				txtproj_items_missing.setText(String.valueOf(result.getItem_missing()));
				revData = result;
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Couldn't retrieve data...something went bad!!!");
				
			}
		};
		dbService.getReviews(pReviewId, callBackAsync);
	}
	
	@UiHandler("bttnSave")
	void onClick(ClickEvent e)
	{
		AsyncCallback<Boolean> callBackAsync=new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				Window.alert("Saved Successfully!");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Something went bad!!!");
			}
		};
	  revData=this.initRevData();
	  if (revData != null)
	  {
		  dbService.saveReviewDetails(revData, callBackAsync);
	  }
	  {
		  if (revData.getMode() == 0)
			  revData = new Reviews();
		  else
			  revData = new Reviews(this.ReviewID);
	  }
	}	
	
	@SuppressWarnings("deprecation")
	public Reviews initRevData()
	{
		try
		{
			java.sql.Date start_date=null;
			if (dpProj_start.getValue() != null)
			{
				start_date= new Date(dpProj_start.getValue().getYear(), dpProj_start.getValue().getMonth(), dpProj_start.getValue().getDate());
				revData.setStart_date(start_date);
			}
			else
			{
				lblError.setText("You can't leave the Date Blank!!!");
				return null;
			}
			revData.setPlan_id(PlanID);
			revData.setOutsourcing_cost(FieldVerifier.StringToNumberDefault0(txtproj_outs_cost.getText()));
			revData.setBought_out_cost(FieldVerifier.StringToNumberDefault0(txtproj_bout_item.getText()));
			revData.setDirect_charge(FieldVerifier.StringToNumberDefault0(txtproj_direct_cost.getText()));
			revData.setOther_cost(FieldVerifier.StringToNumberDefault0(txtproj_other_cost.getText()));
			revData.setNo_of_resources(FieldVerifier.StringToNumberDefault0(txtproj_resources.getText()));
			revData.setDuration(FieldVerifier.StringToNumberDefault0(txtproj_days.getText()));
			revData.setItem_ok(FieldVerifier.StringToNumberDefault0(txtproj_items_ok.getText()));
			revData.setItem_not_ok(FieldVerifier.StringToNumberDefault0(txtproj_items_not_ok.getText()));
			revData.setItem_missing(FieldVerifier.StringToNumberDefault0(txtproj_items_missing.getText()));
			lblError.setText("It's OK - " + start_date.toString() + ", " + String.valueOf(revData.getOutsourcing_cost()));
		}
		catch (Exception ex)
		{
			lblError.setText("Something bad happened. Have you filled up all the fields???");
		}
		return revData;
	}
	public void setText(String text) {
		
	}

	public String getText() {
		return "";
	}

}
