package com.rahul.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.rahul.client.service.GroupManagement;
import com.rahul.model.MGroup;
import com.rahul.model.MProject;
import com.rahul.server.dbqueries.Mqueries;
import com.rahul.server.utils.MySQLAccess;

public class GroupManagementImpl extends RemoteServiceServlet implements GroupManagement {

	private static final long serialVersionUID = 1L;
	private Connection dbCon;
	
	@Override
	public MGroup[] getGroups() {
		dbCon = MySQLAccess.getConnection();
		
		List<MGroup> retVal = new ArrayList<MGroup>();
		try 
		{
			ResultSet rs = Mqueries.getResult(dbCon, "SELECT group_id, group_desc, own_user_id, state, c_dttm, u, g, o FROM ac_group");
			while (rs.next())
			{
				MGroup tmp = new MGroup();
				tmp.setGroup_id(Long.parseLong(rs.getString("group_id")));
				tmp.setGroup_desc(rs.getString("group_desc"));
				retVal.add(tmp);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retVal.toArray(new MGroup[retVal.size()]);
	}
}
