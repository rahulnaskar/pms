package com.rahul.server;

import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.rahul.client.service.LaunchControl;
import com.rahul.server.dbqueries.Mqueries;
import com.rahul.server.utils.MySQLAccess;

public class LaunchControlImpl extends RemoteServiceServlet implements LaunchControl {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String m_User_Name;
	private String m_User_ID;
	private Connection dbCon;
	
	
	/* Does the login at server*/
	@Override
	public boolean doLogin(String username, String password) {
		boolean retVal = false;
		m_User_Name = null;
		m_User_ID = null;
		
		dbCon = MySQLAccess.getConnection();
		
		
		try 
		{
			ResultSet rs = Mqueries.getResult(dbCon, "Select * from ac_user");;
			while (rs.next())
			{
				//System.out.println(username + ", " + password);
				//System.out.println("User ID is" + rs.getString("user_id"));
				if (rs.getString("user_id").equalsIgnoreCase(username))
				{
					m_User_ID = rs.getString("user_id");
					m_User_Name = rs.getString("user_name");
					if (rs.getString("user_pwd").equalsIgnoreCase(password))
					{
						retVal = true;
						// create session and store userid
						HttpServletRequest request = this.getThreadLocalRequest();
						//true will create a new session if it not yet exists
						HttpSession session = request.getSession(true);
						session.setAttribute("UserID", m_User_ID);
					}
					break;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retVal;
	}

	public boolean checkLogin() 
	{
		HttpServletRequest request = this.getThreadLocalRequest();
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("UserID") == null)
			return false;
		return true;
	}

	public void logout() {
 
		HttpServletRequest request = this.getThreadLocalRequest();
		HttpSession session = request.getSession(false);
		if (session == null)
			return;
		session.invalidate();
	}	
	
	@Override
	public int addTwoNumber(int num1, int num2) {
		return (num1 + num2);
	}

	@Override
	public String getUserName() 
	{
		String retVal = null;
		dbCon = MySQLAccess.getConnection();
		HttpServletRequest request = this.getThreadLocalRequest();
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("UserID") == null)
			return retVal;
		try 
		{
			ResultSet rs = Mqueries.getResult(dbCon, "Select user_name from ac_user where user_id='" + session.getAttribute("UserID") + "'");
			while (rs.next())
			{
				retVal = rs.getString("user_name"); 
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retVal;
	}

	@Override
	public String getUserID() {
		return m_User_ID;	
	}

}
