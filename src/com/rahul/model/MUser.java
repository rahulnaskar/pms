package com.rahul.model;

import java.io.Serializable;

public class MUser implements Serializable {
	private String  user_id;
	private String  user_name;
	private String  user_pwd;
	private String  Logged;
	private String  Logged_dttm;
	private String  Last_activity;
	private String  current_activity;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getLogged() {
		return Logged;
	}
	public void setLogged(String logged) {
		Logged = logged;
	}
	public String getLogged_dttm() {
		return Logged_dttm;
	}
	public void setLogged_dttm(String logged_dttm) {
		Logged_dttm = logged_dttm;
	}
	public String getLast_activity() {
		return Last_activity;
	}
	public void setLast_activity(String last_activity) {
		Last_activity = last_activity;
	}
	public String getCurrent_activity() {
		return current_activity;
	}
	public void setCurrent_activity(String current_activity) {
		this.current_activity = current_activity;
	}


}
