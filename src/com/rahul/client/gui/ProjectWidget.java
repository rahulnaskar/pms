package com.rahul.client.gui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.rahul.client.service.LaunchControl;
import com.rahul.client.service.Project;
import com.rahul.client.service.ProjectAsync;
import com.rahul.client.utils.PasswordTextBoxWithLabel;
import com.rahul.client.utils.TextBoxWithLabel;
import com.rahul.model.MProject;

public class ProjectWidget extends Composite implements AsyncCallback, ProjectAsync {

	private ProjectAsync service;
	String url;
	AsyncCallback callBack;
	
	private VerticalPanel vPanel = new VerticalPanel();
	private TextBox txtName;
	private PasswordTextBox txtPassword;
	private Button cmdSave;
	private Label result;

	public ProjectWidget(String purl) 
	{
		this.callBack = null;
		init(purl);
	}	
	
	public ProjectWidget(String purl, AsyncCallback pCallback) 
	{
		this.callBack = pCallback;
		init(purl);
	}
	
	private void init(String purl)
	{
		vPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		initWidget(this.vPanel);
		
		url = purl + "projectservice";
		
		this.service = GWT.create(Project.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);
		
		
		
		this.txtName = new TextBoxWithLabel("Project Name");
		this.vPanel.add(txtName);
		txtName.setSize("181", "31");
		this.txtPassword = new PasswordTextBoxWithLabel("Password");
		//this.vPanel.add(txtPassword);
		cmdSave = new Button("Press to see the projects");
		cmdSave.addClickHandler(new cmdSaveClickHandler());
		this.vPanel.add(cmdSave);
		cmdSave.setSize("", "");
		result = new Label("");
		this.vPanel.add(result);
	}

	private class cmdSaveClickHandler implements ClickHandler
	{

		@Override
		public void onClick(ClickEvent event) {
			getProjects(ProjectWidget.this);
		}
		
	}
	
	@Override
	public void getProjects(AsyncCallback callback) {
		this.service.getProjects(this);
		
	}

	@Override
	public void onFailure(Throwable caught) {
		this.result.setText("Something has gone wrong");
		
	}

	@Override
	public void onSuccess(Object result) {
		if (result.getClass().isArray())
		{
			MProject[] tmp = (MProject[])result;
			if (tmp.length > 0)
			{
				txtName.setText(tmp[0].getP_name());
			}
		}
		
	}

}
