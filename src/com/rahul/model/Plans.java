package com.rahul.model;

import java.io.Serializable;
import java.util.Date;

public class Plans implements Serializable {
	
	private int plan_id;
	private int project_id;
	private String project_name;
	private String phase;
	private int outsourcing_cost;
	private int bought_out_items;
	private int additional_cost;
	private int no_of_resources;
	private int days;
	private Date start_date;
	private Date end_date;
	private int customer_suppled_item;
	
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public int getProject_id() {
		return project_id;
	}
	public String getPhase() {
		return phase;
	}
	public int getOutsourcing_cost() {
		return outsourcing_cost;
	}
	public int getBought_out_items() {
		return bought_out_items;
	}
	public int getAdditional_cost() {
		return additional_cost;
	}
	public int getNo_of_resources() {
		return no_of_resources;
	}
	public Date getStart_date() {
		return start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public int getCustomer_suppled_item() {
		return customer_suppled_item;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public void setOutsourcing_cost(int outsourcing_cost) {
		this.outsourcing_cost = outsourcing_cost;
	}
	public void setBought_out_items(int bought_out_items) {
		this.bought_out_items = bought_out_items;
	}
	public void setAdditional_cost(int additional_cost) {
		this.additional_cost = additional_cost;
	}
	public void setNo_of_resources(int no_of_resources) {
		this.no_of_resources = no_of_resources;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public void setCustomer_suppled_item(int customer_suppled_item) {
		this.customer_suppled_item = customer_suppled_item;
	}

}
