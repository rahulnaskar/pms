package com.rahul.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.rahul.model.MUser;

public interface UserManagementAsync {

	void addUser(MUser mUser, AsyncCallback<Boolean> callback);

}
