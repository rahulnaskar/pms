package com.rahul.shared;

public class FieldVerifier {

        public static boolean isValidLength(String name, int len) {
                if (name == null) {
                        return false;
                }
                return name.length() >= len;
        }
        public static boolean isStringsSame(String one, String two) {
            if (one == null || two == null) {
                    return false;
            }
            return one.equals(two);
    }  
        
        
        public static int StringToNumberDefault0(String myString)
        {
        	if (FieldVerifier.EmptyStringFromNull(myString).length() == 0)
        		return new Integer(0);
        	else
        		return Integer.parseInt(myString);
        }
        public static String EmptyStringFromNull(String myString)
        {
        	if (myString == null)
        		return new String("");
        	else
        		return myString;
        }
        
        
}
