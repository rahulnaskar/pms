package com.rahul.model;

import java.io.Serializable;

import com.google.gwt.i18n.client.DateTimeFormat;

public class MProject implements Serializable {
	private int p_id;
	private String p_code;
	private String p_name;
	private String created_by;
	//private DateTimeFormat created_dttm = new DateTimeFormat();
	private String modified_by;
	//private DateTimeFormat modified_dttm;
	public int getP_id() {
		return p_id;
	}
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}
	public String getP_code() {
		return p_code;
	}
	public void setP_code(String p_code) {
		this.p_code = p_code;
	}
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
/*	public DateTimeFormat getCreated_dttm() {
		return created_dttm;
	}
	public void setCreated_dttm(DateTimeFormat created_dttm) {
		this.created_dttm = created_dttm;
	}*/
	public String getModified_by() {
		return modified_by;
	}
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}
/*	public DateTimeFormat getModified_dttm() {
		return modified_dttm;
	}
	public void setModified_dttm(DateTimeFormat modified_dttm) {
		this.modified_dttm = modified_dttm;
	}*/
	public float getState_id() {
		return state_id;
	}
	public void setState_id(float state_id) {
		this.state_id = state_id;
	}
	private float state_id;
}
