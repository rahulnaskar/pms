package com.rahul.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.rahul.client.gui.Login;

public class LandingPage extends Composite implements HasText, EntryPoint, AsyncCallback {

	private static LandingPageUiBinder uiBinder = GWT
			.create(LandingPageUiBinder.class);

	interface LandingPageUiBinder extends UiBinder<Widget, LandingPage> {
	}


	@UiField
	Login u_login;
	@UiField
	StackLayoutPanel u_left_menu;
	
	public LandingPage() {
		initWidget(uiBinder.createAndBindUi(this));
		this.u_left_menu.setVisible(false);
	}

	public LandingPage(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		u_login.setCallBackHandler(this);
		this.u_left_menu.setVisible(false);
	}

	public void setText(String text) {
		//button.setText(text);
	}

	public String getText() {
		//return button.getText();
		return null;
	}

	@Override
	public void onModuleLoad() {
		LandingPage w = new LandingPage("Test");
		RootLayoutPanel.get().add(w);
	}

	@Override
	public void onFailure(Throwable caught) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSuccess(Object result) {
		if (result instanceof Boolean)
		{
			if ((Boolean)result)
			{
				this.u_left_menu.setVisible(false);
				
			}
				
		}
	}

}
