package com.rahul.client.gui;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;
import com.rahul.model.Plans;
import com.rahul.model.Reviews;
import com.rahul.model.Reviews;

public class ReviewGridWidget extends Composite implements HasText {

	private static ReviewGridWidgetUiBinder uiBinder = GWT
			.create(ReviewGridWidgetUiBinder.class);
	private Reviews[] reviewData;
	private HTMLPanel mainPanel;
    private Plans planData;
	interface ReviewGridWidgetUiBinder extends
			UiBinder<Widget, ReviewGridWidget> {
	}

	public ReviewGridWidget() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	
	public ReviewGridWidget(Reviews[] pReviewData,HTMLPanel pHtmlPanel,Plans pPlans) {
		initWidget(uiBinder.createAndBindUi(this));
		reviewData=pReviewData;
		mainPanel=pHtmlPanel;
		planData=pPlans;
		populateReviewGrid(reviewData);
	}
	
	public ReviewGridWidget(Reviews[] pReviewData,HTMLPanel pHtmlPanel) {
		initWidget(uiBinder.createAndBindUi(this));
		reviewData=pReviewData;
		mainPanel=pHtmlPanel;
		
		populateReviewGrid(reviewData);
	}

	@UiField CellTable<Reviews> cellTable;
	@UiField Button bttnAdd;
	@UiHandler("bttnAdd")
	void onClick(ClickEvent e)
	{
		mainPanel.clear();
		FrmReview reviewFrm=new FrmReview(planData.getPlan_id(),0);
		Widget reviewFrmWdg=reviewFrm.asWidget();
		mainPanel.add(reviewFrmWdg);
		
		
		
		
	}
	
	private void populateReviewGrid(Reviews[] pReview)
	{
this.cellTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	   
	    
	    NumberCell ReviewIdNumCell=new NumberCell();
	    Column<Reviews,Number> ReviewIdNumColumn=new Column<Reviews,Number>(ReviewIdNumCell) {

			@Override
			public Number getValue(Reviews object) {
				// TODO Auto-generated method stub
				return object.getReview_id();
			}
	    	
		};
		cellTable.addColumn(ReviewIdNumColumn,"Reviews ID");
		
		
		
		 NumberCell planIdCell=new NumberCell();
		    Column<Reviews,Number> planIdColumn=new Column<Reviews,Number>(planIdCell) {

				@Override
				public Number getValue(Reviews object) {
					// TODO Auto-generated method stub
					return object.getPlan_id();
				}
		    	
			};
			cellTable.addColumn(planIdColumn,"Plan ID");
			

			 NumberCell outsCostCell=new NumberCell();
			    Column<Reviews,Number> projoutsCostColumn=new Column<Reviews,Number>(outsCostCell) {

					@Override
					public Number getValue(Reviews object) {
						// TODO Auto-generated method stub
						return object.getOutsourcing_cost();
					}
			    	
				};
				cellTable.addColumn(projoutsCostColumn,"Outsourcing Cost");
				
				
				
				 NumberCell boutCell=new NumberCell();
				    Column<Reviews,Number> projboutColumn=new Column<Reviews,Number>(boutCell) {

						@Override
						public Number getValue(Reviews object) {
							// TODO Auto-generated method stub
							return object.getBought_out_cost();
						}
				    	
					};
					cellTable.addColumn(projboutColumn,"Bought out Cost");
		

					 NumberCell directChargeCell=new NumberCell();
					    Column<Reviews,Number> directChargeColumn=new Column<Reviews,Number>(directChargeCell) {

							@Override
							public Number getValue(Reviews object) {
								// TODO Auto-generated method stub
								return object.getDirect_charge();
							}
					    	
						};
						cellTable.addColumn(directChargeColumn,"Direct Charge");
						
						
						 NumberCell otherChargeCell=new NumberCell();
						    Column<Reviews,Number> otherChargeColumn=new Column<Reviews,Number>(otherChargeCell) {

								@Override
								public Number getValue(Reviews object) {
									// TODO Auto-generated method stub
									return object.getOther_cost();
								}
						    	
							};
							cellTable.addColumn(otherChargeColumn,"Other Charge");
							
							NumberCell resourcesCell=new NumberCell();
						    Column<Reviews,Number> resourcesColumn=new Column<Reviews,Number>(resourcesCell) {

								@Override
								public Number getValue(Reviews object) {
									// TODO Auto-generated method stub
									return object.getNo_of_resources();
								}
						    	
							};
							cellTable.addColumn(resourcesColumn,"No of Resources");
							
							
							NumberCell durationCell=new NumberCell();
						    Column<Reviews,Number> durationColumn=new Column<Reviews,Number>(durationCell) {

								@Override
								public Number getValue(Reviews object) {
									// TODO Auto-generated method stub
									return object.getDuration();
								}
						    	
							};
							cellTable.addColumn(durationColumn,"Duration");
							
							NumberCell itemOkCell=new NumberCell();
						    Column<Reviews,Number> itemOkColumn=new Column<Reviews,Number>(itemOkCell) {

								@Override
								public Number getValue(Reviews object) {
									// TODO Auto-generated method stub
									return object.getItem_ok();
								}
						    	
							};
							cellTable.addColumn(itemOkColumn,"Item Ok");
							
							NumberCell itemNotOkCell=new NumberCell();
						    Column<Reviews,Number> itemNotOkColumn=new Column<Reviews,Number>(itemNotOkCell) {

								@Override
								public Number getValue(Reviews object) {
									// TODO Auto-generated method stub
									return object.getItem_not_ok();
								}
						    	
							};
							cellTable.addColumn(itemNotOkColumn,"Item Not Ok");
							
							
							
							NumberCell itemMissingCell=new NumberCell();
						    Column<Reviews,Number> itemMissingColumn=new Column<Reviews,Number>(itemMissingCell) {

								@Override
								public Number getValue(Reviews object) {
									// TODO Auto-generated method stub
									return object.getItem_missing();
								}
						    	
							};
							cellTable.addColumn(itemMissingColumn,"Item Missing");
	    
	    
	    

	    // Add a date column to show the birthday.
	    DateCell StartdateCell = new DateCell();
	    Column<Reviews, Date> StartdateColumn = new Column<Reviews, Date>(StartdateCell) {
	      @Override
	      public Date getValue(Reviews object) {
	        return object.getStart_date();
	      }
	    };
	    cellTable.addColumn(StartdateColumn, "Start Date");
	    
	    DateCell EndDateCell = new DateCell();
	    Column<Reviews, Date> EndDateColumn = new Column<Reviews, Date>(EndDateCell) {
	      @Override
	      public Date getValue(Reviews object) {
	        return object.getEnd_date();
	      }
	    };
	    cellTable.addColumn(EndDateColumn, "End Date");
	    
	    ButtonCell editButton=new ButtonCell();
	    Column<Reviews, String> buttonColumn = new Column<Reviews, String>(editButton) {
			
			@Override
			public String getValue(Reviews object) {
				return "Review";
			}
		};
	    	   
        cellTable.addColumn(buttonColumn,"Review");
	    
	    cellTable.setRowCount(1, true);

	    // Push the data into the widget.
	    List<Reviews> REVIEWS=Arrays.asList(pReview);
	    cellTable.setRowData(0,REVIEWS);

	    // Add it to the root panel.
	   // RootPanel.get().add(table);
	}

	public ReviewGridWidget(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		//button.setText(firstName);
	}

	

	public void setText(String text) {
		//button.setText(text);
	}

	public String getText() {
		return "";
	}

}
