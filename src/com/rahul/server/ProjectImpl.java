package com.rahul.server;

import java.sql.Connection;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.rahul.client.service.Project;
import com.rahul.model.MProject;
import com.rahul.server.dbqueries.Mqueries;
import com.rahul.server.utils.MySQLAccess;

public class ProjectImpl extends RemoteServiceServlet implements Project {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection dbCon;
	
	
	@Override
	public MProject[] getProjects() {
		Connection dbCon = MySQLAccess.getConnection();
		
		List<MProject> retVal = new ArrayList<MProject>();
		try 
		{
			ResultSet rs = Mqueries.getResult(dbCon, "SELECT p_id, p_code, p_name, created_by, created_dttm, modified_by, modified_dttm, state_id FROM m_project");;
			while (rs.next())
			{
				MProject tmp = new MProject();
				tmp.setP_id(Integer.parseInt(rs.getString("p_id")));
				tmp.setP_code(rs.getString("p_code"));
				tmp.setP_name(rs.getString("p_name"));
				
				retVal.add(tmp);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retVal.toArray(new MProject[retVal.size()]);

		
	}

}
