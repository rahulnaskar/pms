package com.rahul.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class FrmProjectDetails extends Composite implements HasText {

	private static FrmProjectDetailsUiBinder uiBinder = GWT
			.create(FrmProjectDetailsUiBinder.class);

	interface FrmProjectDetailsUiBinder extends
			UiBinder<Widget, FrmProjectDetails> {
	}

	public FrmProjectDetails() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiField
	Button button;

	public FrmProjectDetails(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		button.setText(firstName);
	}

	@UiHandler("button")
	void onClick(ClickEvent e) {
		Window.alert("Hello!");
	}
	
	
	@UiField
	Button button1;

	public void setText(String text) {
		button.setText(text);
	}

	public String getText() {
		return button.getText();
	}

}
