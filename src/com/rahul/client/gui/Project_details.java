package com.rahul.client.gui;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.thirdparty.streamhtmlparser.HtmlParser.Mode;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.rahul.client.service.DbCommService;
import com.rahul.client.service.DbCommServiceAsync;
import com.rahul.model.Plans;
import com.rahul.model.Projects;

public class Project_details extends Composite implements HasText {

	private static Project_detailsUiBinder uiBinder = GWT
			.create(Project_detailsUiBinder.class);
	private Projects[] projectList=null;
	HTMLPanel mainPanel;
	private  DbCommServiceAsync dbService;
	String url;
	Plans[] plansD=null;

	interface Project_detailsUiBinder extends UiBinder<Widget, Project_details> {
	}

	public Project_details() {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
	}
	public Project_details(Projects[] pProject,HTMLPanel pMainPanel) {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		projectList=pProject;
		this.populateGrid(projectList);
		mainPanel=pMainPanel;
	}
	@UiField
	CellTable<Projects> cellTable;
	@UiField SimplePager pager;
	@UiField Button bttnAdd;
	
	@UiHandler("bttnAdd")
	void onClick(ClickEvent e)
	{
		this.setVisible(false);
		FrmProject prjFrm=new FrmProject(false,null);
		Widget prjFrmWd=prjFrm.asWidget();
		//RootPanel.get("test").add(prjFrmWd);
		mainPanel.add(prjFrmWd);
	}
	
	
    
  Plans[] getPlanGridDataByProjId(int pPid)
  {
	 final Plans[] plansD=null;
	  AsyncCallback<Plans[]> plansCallBack=new AsyncCallback<Plans[]>() {
		
		@Override
		public void onSuccess(Plans[] result) {
			
			PlansGridWidget PlanGrid=new PlansGridWidget(result,mainPanel);
        	Widget PlanGridWdg=PlanGrid.asWidget();
        	mainPanel.add(PlanGridWdg);	
			
		}
		
		
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("faliure");
			
		}
	};
	dbService.getPlansGridData(pPid, plansCallBack);
	return plansD;
  }
	
	
	
	
    
	public void populateGrid(Projects[] pList)
	{
		
		
		  final SingleSelectionModel<Projects> selectionModel
		     = new SingleSelectionModel<Projects>();

		   cellTable.setSelectionModel(selectionModel);
		   cellTable.addDomHandler(new DoubleClickHandler() {

		        @Override
		        public void onDoubleClick(final DoubleClickEvent event) {
		            Projects selected = selectionModel.getSelectedObject();
		             
		            if (selected != null) {
		            	//clear the main panel
		            	mainPanel.clear();
		            	FrmProject prjFrm=new FrmProject(true,selected);
		            	Widget prjFrmWidget=prjFrm.asWidget();
		            	
		            	mainPanel.add(prjFrmWidget);
		            	getPlanGridDataByProjId(selected.getProject_id());
		            	
		            	

		                                  }

		        }

				
		    }, 
		    DoubleClickEvent.getType());
		
		 
		    this.cellTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		    

		    // Add a text column to show the name.
		    NumberCell idnum=new NumberCell();
		    Column<Projects,Number> projIdColumn=new Column<Projects,Number>(idnum) {

				@Override
				public Number getValue(Projects object) {
					// TODO Auto-generated method stub
					return object.getProject_id();
				}
		    	
			};
			cellTable.addColumn(projIdColumn,"ID");
		    
		    
		    TextColumn<Projects> nameColumn = new TextColumn<Projects>() {
		      @Override
		      public String getValue(Projects object) {
		        return object.getProject_name();
		      }
		    };
		    cellTable.addColumn(nameColumn, "Name");
		    
		    
			
			 NumberCell projValue=new NumberCell();
			    Column<Projects,Number> projValueColumn=new Column<Projects,Number>(projValue) {

					@Override
					public Number getValue(Projects object) {
						// TODO Auto-generated method stub
						return object.getProj_value();
					}
			    	
				};
				cellTable.addColumn(projValueColumn,"Value");
				

				 NumberCell projCost=new NumberCell();
				    Column<Projects,Number> projCostColumn=new Column<Projects,Number>(projCost) {

						@Override
						public Number getValue(Projects object) {
							// TODO Auto-generated method stub
							return object.getProj_cost();
						}
				    	
					};
					cellTable.addColumn(projCostColumn,"Cost");
					
					 NumberCell projDuration=new NumberCell();
					    Column<Projects,Number> projDurationColumn=new Column<Projects,Number>(projDuration) {

							@Override
							public Number getValue(Projects object) {
								// TODO Auto-generated method stub
								return object.getProj_duration();
							}
					    	
						};
						cellTable.addColumn(projDurationColumn,"Duration");
			
			
		    
		    

		    // Add a date column to show the birthday.
		    DateCell StartdateCell = new DateCell();
		    Column<Projects, Date> StartdateColumn = new Column<Projects, Date>(StartdateCell) {
		      @Override
		      public Date getValue(Projects object) {
		        return object.getProj_start_date();
		      }
		    };
		    cellTable.addColumn(StartdateColumn, "Start Date");
		    
		    DateCell EndDateCell = new DateCell();
		    Column<Projects, Date> EndDateColumn = new Column<Projects, Date>(EndDateCell) {
		      @Override
		      public Date getValue(Projects object) {
		        return object.getProj_end_date();
		      }
		    };
		    cellTable.addColumn(EndDateColumn, "End Date");

		    
		    cellTable.setRowCount(1, true);

		    // Push the data into the widget.
		    List<Projects> PROJECTS=Arrays.asList(pList);
		    cellTable.setRowData(0,PROJECTS);
		    pager.setPageSize(2);

		    // Add it to the root panel.
		   // RootPanel.get().add(table);

		
	}
	
	public Project_details(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		//button.setText(firstName);
	}

	

	public void setText(String text) {
		//button.setText(text);
	}

	public String getText() {
		return "hi";
	}

}
