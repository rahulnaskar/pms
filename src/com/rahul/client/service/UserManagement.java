package com.rahul.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.rahul.model.MUser;

@RemoteServiceRelativePath("usermanagementservice")
public interface UserManagement extends RemoteService{
	boolean addUser(MUser mUser);
}
