package com.rahul.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.rahul.model.*;

@RemoteServiceRelativePath("databaseService")
public interface DbCommService  extends RemoteService{
	Projects[] getProjectList(int id);
	Plans[] getPlansGridData(int pProjId);
	boolean savePlanDetails(Plans pPlans,Boolean editMode);
	Boolean saveProjectDetails(Projects pProj,Boolean editMode);
	String[] getProjComboList();
	Reviews getReviews(int pReviewID);
	boolean saveReviewDetails(Reviews pRev);
	Reviews[] getReviewsByPlan(int pPlanId);
	
	String getName();

}
