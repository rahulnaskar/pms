package com.rahul.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.rahul.model.MGroup;

@RemoteServiceRelativePath("groupmanagementservice")
public interface GroupManagement extends RemoteService {
	public MGroup[] getGroups();
}
