package com.rahul.client.gui;

import java.sql.Date;
import java.text.SimpleDateFormat;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.rahul.client.service.DbCommService;
import com.rahul.client.service.DbCommServiceAsync;
import com.rahul.model.Projects;

public class FrmProject extends Composite implements HasText {

	private static FrmProjectUiBinder uiBinder = GWT
			.create(FrmProjectUiBinder.class);
	private Projects projData=new Projects();
	private  DbCommServiceAsync dbService=null;
	String url;
	Boolean editMode=false;

	interface FrmProjectUiBinder extends UiBinder<Widget, FrmProject> {
	}

	public FrmProject(Boolean pMode,Projects pProject) {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		editMode=pMode;
		projData=pProject;
		dpProj_start.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
		if(editMode)
		{
			this.assignFormField(projData);
			this.bttnSave.setText("Update");
		}
	}

	

	public FrmProject(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		//button.setText(firstName);
		 
		 
	}
	
	
@UiField TextBox txtproj_name;

@UiField TextBox txtproj_value;
@UiField TextBox txtproj_cost;
@UiField TextBox txtproj_days;
@UiField DateBox dpProj_start;
@UiField Button bttnSave;

@UiHandler("bttnSave")
void onClick(ClickEvent e)
{
	
	
	AsyncCallback<Boolean> callBackAsync=new AsyncCallback<Boolean>() {
		
		@Override
		public void onSuccess(Boolean result) {
			// TODO Auto-generated method stub
			Window.alert("Data Saved successfully");
		}
		
		@Override
		public void onFailure(Throwable caught) {
			
			Window.alert("Error in saving data");
		}
	};
	//dbService.getName(callback);
	
	projData=this.initProjData();	
	
  dbService.saveProjectDetails(projData, editMode,callBackAsync);
}

//	@UiHandler("button")
//	void onClick(ClickEvent e) {
//		Window.alert("Hello!");
//	}
@SuppressWarnings("deprecation")
private Projects initProjData()
{
	try
	{
	if(editMode==false)
	{
		projData=new Projects();
		
	}
		
	Date start_date=new Date(dpProj_start.getValue().getYear(), dpProj_start.getValue().getMonth(), dpProj_start.getValue().getDate());
	projData.setProject_name(txtproj_name.getText());
	projData.setProj_value(Integer.parseInt(txtproj_value.getText()));
	projData.setProj_cost(Integer.parseInt(txtproj_cost.getText()));
	projData.setProj_duration(Integer.parseInt(txtproj_days.getText()));
	//String start_date= new Date(dpProj_start.getValue().getYear(), dpProj_start.getValue().getMonth(), dpProj_start.getValue().getDate());
	
	
	projData.setProj_start_date(start_date);
	
	}
	catch(Exception e)
	{
		
	}
	return projData;
}
	
private void assignFormField(Projects pProject)
{
	txtproj_name.setText(pProject.getProject_name());
	txtproj_cost.setText(Integer.toString(pProject.getProj_cost()));
	txtproj_days.setText(Integer.toString(pProject.getProj_duration()));
	txtproj_value.setText(Integer.toString(pProject.getProj_value()));
	dpProj_start.setValue(pProject.getProj_start_date());
	
}

	public void setText(String text) {
		//passwd.setText(text);
	}

	public String getText() {
		return "hi";
	}

}
