package com.rahul.server.dbqueries;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.rahul.model.*;


public class DataProvider {
	
	private Connection dbCon;

	public DataProvider(Connection pCon) {
		
		dbCon=pCon;
	}
	
	
	
	public Projects[] getProjectDetails(int pId) throws Exception
	{
		
		List<Projects> projectList=new ArrayList<Projects>();
		Statement stmnt=dbCon.createStatement();
		ResultSet rs=null;
		if(pId==0)
		{
			 rs=stmnt.executeQuery("select * from tbl_project_master");
		}
		else
		{
			//rs=stmnt.executeQuery("select * from tbl_project_master where ");
		}
		while(rs.next())
		{
			Projects prj=new Projects();
			prj.setProject_id(rs.getInt("P_ID"));
			prj.setProject_name(rs.getString("project_name"));
			prj.setProj_value(rs.getInt("proj_value"));
			prj.setProj_cost(rs.getInt("proj_cost"));
			prj.setProj_duration(rs.getInt("proj_duration"));
			prj.setProj_start_date(rs.getDate("proj_start_date"));
			prj.setProj_end_date(rs.getDate("proj_end_date"));
			projectList.add(prj);
			
		}
		
		return projectList.toArray(new Projects[projectList.size()]);
		
	}
	public Plans[] getPlanData(int pPid) throws SQLException
	{
		List<Plans> planList=new ArrayList<Plans>();
		Statement stmnt=dbCon.createStatement();
		ResultSet rs=null;
		if(pPid==0)
		{
			rs=stmnt.executeQuery("select * from tbl_plan_master");
		}
		else
		{
			rs=stmnt.executeQuery("select * from tbl_plan_master where project_id="+pPid);
		}
		
		while(rs.next())
		{
			Plans planData=new Plans();
			planData.setPlan_id(rs.getInt("plan_id"));
			planData.setProject_id(rs.getInt("project_id"));
			planData.setPhase(rs.getString("phase"));
			planData.setOutsourcing_cost(rs.getInt("outsourcing_cost"));
			planData.setBought_out_items(rs.getInt("bought_out_items"));
			planData.setAdditional_cost(rs.getInt("additional_cost"));
			planData.setNo_of_resources(rs.getInt("no_of_resources"));
			planData.setStart_date(rs.getDate("start_date"));
			planData.setEnd_date(rs.getDate("end_date"));
			planData.setCustomer_suppled_item(rs.getInt("customer_supplied_item"));
			planList.add(planData);
			
		}
		
		return planList.toArray(new Plans[planList.size()]);
		
	}
	public Boolean saveProject(Projects pProject,Boolean editMode)
	{
		Boolean result=false;
		try {
			Statement stmnt=dbCon.createStatement();
			//@SuppressWarnings("deprecation")
			//String start_date= DateTimeFormat.getShortDateFormat().format(pProject.getProj_start_date());
			//String start_date= DateTimeFormat.getShortDateFormat().format(pProject.getProj_start_date());
			String insertQ="insert into tbl_project_master(project_name,proj_value,proj_cost,proj_duration,proj_start_date,proj_end_date) values('"+pProject.getProject_name()+"',"+pProject.getProj_value()+","+pProject.getProj_cost()+","+pProject.getProj_duration()+",'"+pProject.getProj_start_date()+"',Date_add('"+pProject.getProj_start_date()+"', Interval "+pProject.getProj_duration()+" Day))";
			String UpdateSql="UPDATE `ntpl_project_management`.`tbl_project_master` SET"+
								"`project_name` = '"+pProject.getProject_name()+"',"
								+"`proj_value` = "+pProject.getProj_value()+","
								+"`proj_cost` = "+pProject.getProj_cost()+","
								+"`proj_duration` ="+pProject.getProj_duration()+","
								+"`proj_start_date` ='"+pProject.getProj_start_date()+"',"
								+"`proj_end_date` = Date_add('"+pProject.getProj_start_date()+"', Interval "+pProject.getProj_duration()+" Day)"
								+" WHERE `p_id` = "+pProject.getProject_id();
			if(editMode)
			{
				stmnt.executeUpdate(UpdateSql);
				
			}
			else
			{
				stmnt.execute(insertQ);
				
			}
			
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}
	
	
	public String[] getListofProjects()
	{
		String[] result=null;
		Statement stmnt;
		ResultSet rs;
		try {
			stmnt = dbCon.createStatement();
			
		    rs=stmnt.executeQuery("select * from tbl_project_master");
		    
		    rs.last(); 
		    result=new String[rs.getRow()];
		    rs.beforeFirst();
		   
		    int i=0;
		    while(rs.next())
		    {
		    	result[i]=rs.getString("project_name");
		    	i++;
		    }
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public Boolean savePlans(Plans pPlans,Boolean editMode)
	{
		Boolean result=false;
		try {
			//getting plan id
			Statement stmnt1=dbCon.createStatement();
			String sql="select P_ID from tbl_project_master where project_name='"+pPlans.getProject_name()+"'";
			ResultSet rs1=stmnt1.executeQuery(sql);
			rs1.next();
			int projId=rs1.getInt("P_ID");
			Statement stmnt=dbCon.createStatement();
			String insertQ="INSERT INTO `ntpl_project_management`.`tbl_plan_master`(`project_id`,`phase`,`outsourcing_cost`,`bought_out_items`,`additional_cost`,`no_of_resources`,`start_date`,`end_date`,`customer_supplied_item`)"+
							"VALUES("+projId +",'"+pPlans.getPhase()+"',"+pPlans.getOutsourcing_cost()+","+pPlans.getBought_out_items()+","+pPlans.getAdditional_cost()+","
							+""+pPlans.getNo_of_resources()+",'"+pPlans.getStart_date()+"',curdate(),"+pPlans.getCustomer_suppled_item()+")";
			
			String updateQ="UPDATE `ntpl_project_management`.`tbl_plan_master`SET"
							+"`project_id` = "+pPlans.getProject_id()+","
							+"`phase` ='"+pPlans.getPhase()+"',"
					        +"`outsourcing_cost` = "+pPlans.getOutsourcing_cost()+","
					         +"`bought_out_items` = "+pPlans.getBought_out_items()+","
					         +"`additional_cost` = "+pPlans.getAdditional_cost()+","
					         +"`no_of_resources` = "+pPlans.getNo_of_resources()+","
					         +"`start_date` = '"+pPlans.getStart_date().toString()+"',"
					         +"`end_date` = curdate(),"
					         +"`customer_supplied_item` = "+pPlans.getCustomer_suppled_item()+""
					         +" WHERE `plan_id` ="+pPlans.getPlan_id();
			
			if(editMode)
			{
				stmnt.executeUpdate(updateQ);
			}
			else
			{
				stmnt.execute(insertQ);
				
			}
			
			
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}
	
	//r
	public boolean saveReview(Reviews pRev)
	{
		Boolean result=false;
		try 
		{
			Statement stmnt=dbCon.createStatement();
			String insertQ = null;
			if (pRev.getMode() == 0)
			{
				insertQ="insert into tbl_project_review ("
						+ "plan_id,"
						+ "start_date,"
						+ "outsourcing_cost,"
						+ "bought_out_cost,"
						+ "direct_charge,"
						+ "other_cost,"
						+ "no_of_resource,"
						+ "duration,"
						+ "item_ok,"
						+ "item_not_ok,"
						+ "item_missing "
						+ ") values ("
						+pRev.getPlan_id()+",'"
						+pRev.getStart_date()+"',"
						+pRev.getOutsourcing_cost()+","
						+pRev.getBought_out_cost()+","
						+pRev.getDirect_charge()+","
						+pRev.getOther_cost()+","
						+pRev.getNo_of_resources()+","
						+pRev.getDuration()+","
						+pRev.getItem_ok()+","
						+pRev.getItem_not_ok()+","
						+pRev.getItem_missing()+""
						+")";
			}
			else
			{
				insertQ = "update tbl_project_review set "
						+ "start_date = '" + pRev.getStart_date()
						+ "', outsourcing_cost = " + String.valueOf(pRev.getOutsourcing_cost())
						+ ", bought_out_cost = " + String.valueOf(pRev.getBought_out_cost())
						+ ", direct_charge = " + String.valueOf(pRev.getDirect_charge())
						+ ", other_cost = " + String.valueOf(pRev.getBought_out_cost())
						+ ", no_of_resource = " + String.valueOf(pRev.getNo_of_resources())
						+ ", duration = " + String.valueOf(pRev.getDuration())
						+ ", item_ok = " + String.valueOf(pRev.getItem_ok())
						+ ", item_not_ok = " + String.valueOf(pRev.getItem_not_ok())
						+ ", item_missing = " + String.valueOf(pRev.getItem_missing())
						+ " where review_id="
						+ String.valueOf(pRev.getReview_id());
			}
			
			stmnt.execute(insertQ);
			result=true;
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}	
	
	public Reviews getReview(int pReviewID) {
		// TODO Auto-generated method stub
		Statement stmnt;
		Reviews prj=new Reviews(pReviewID);
		try 
		{
			stmnt = dbCon.createStatement();
			ResultSet rs=stmnt.executeQuery("select * from tbl_project_review where review_id=" + String.valueOf(pReviewID));
			while(rs.next())
			{
				
				prj.setPlan_id(rs.getInt("plan_id"));
				prj.setStart_date(rs.getDate("start_date"));
				prj.setOutsourcing_cost(rs.getInt("outsourcing_cost"));
				prj.setBought_out_cost(rs.getInt("bought_out_cost"));
				prj.setDirect_charge(rs.getInt("direct_charge"));
				prj.setOther_cost(rs.getInt("other_cost"));
				prj.setNo_of_resources(rs.getInt("no_of_resource"));
				prj.setDuration(rs.getInt("duration"));
				prj.setItem_ok(rs.getInt("item_ok"));
				prj.setItem_not_ok(rs.getInt("item_not_ok"));
				prj.setItem_missing(rs.getInt("item_missing"));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return prj;
	}
	
	
	public Reviews[] getReviewsAgainstPlan(int pPlanId)
	{
		Statement stmnt;
		List<Reviews> reviewList=new ArrayList<Reviews>();
	    
	try 
	{   ResultSet rs;
		stmnt = dbCon.createStatement();
		String QueryAll="select * from tbl_project_review";
		String QbyPlan="select * from tbl_project_review where plan_id=" + String.valueOf(pPlanId);
		if(pPlanId==0)
		{
			rs=stmnt.executeQuery(QueryAll);
		}
		else
		{
			rs=stmnt.executeQuery(QbyPlan);
		}
		
		while(rs.next())
		{
			Reviews prj=new Reviews();
			
			prj.setPlan_id(rs.getInt("plan_id"));
			prj.setReview_id(rs.getInt("review_id"));
			prj.setStart_date(rs.getDate("start_date"));
			prj.setOutsourcing_cost(rs.getInt("outsourcing_cost"));
			prj.setBought_out_cost(rs.getInt("bought_out_cost"));
			prj.setDirect_charge(rs.getInt("direct_charge"));
			prj.setOther_cost(rs.getInt("other_cost"));
			prj.setNo_of_resources(rs.getInt("no_of_resource"));
			prj.setDuration(rs.getInt("duration"));
			prj.setItem_ok(rs.getInt("item_ok"));
			prj.setItem_not_ok(rs.getInt("item_not_ok"));
			prj.setItem_missing(rs.getInt("item_missing"));
			reviewList.add(prj);
		}
	} 
	catch (SQLException e) 
	{
		e.printStackTrace();
	}
	return reviewList.toArray(new Reviews[reviewList.size()]);
		
		
	}
	
	
}