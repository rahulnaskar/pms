package com.rahul.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import com.rahul.client.service.*;
import com.rahul.model.*;

public class pageAfterLogin extends Composite implements AsyncCallback, DbCommServiceAsync {

	private static pageAfterLoginUiBinder uiBinder = GWT
			.create(pageAfterLoginUiBinder.class);
	
	private final DbCommServiceAsync dbService;
	String url;
	Projects[] projs=null;
	Plans[] plansD=null;
	AsyncCallback callBack;
	private Label result;

	interface pageAfterLoginUiBinder extends UiBinder<Widget, pageAfterLogin> {
	}

	public pageAfterLogin() {
		initWidget(uiBinder.createAndBindUi(this));
		
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		this.callBack = null;
		//this.result = new Label("Result will come here");
	}

	
	@UiField
	HTMLPanel htmlPanel;
	
	@UiField
	Tree tree;
	@SuppressWarnings("deprecation")
	@UiHandler("tree")
	
	
	
	public void onSelection(SelectionEvent<TreeItem> e)
	{   
		String html=e.getSelectedItem().getHTML();
		if(e.getSelectedItem().getHTML().equals("Projects"))
			
		{
			htmlPanel.clear();
			this.getProjectList(0,this);
			Project_details pd=new Project_details();
			Widget pdWidget=pd.asWidget();
			htmlPanel.add(pdWidget);
			htmlPanel.add(result);
					
				
		}
		else if(e.getSelectedItem().getHTML().equals("Plans"))
		{
			AsyncCallback<Plans[]> plansGridDataCallBack=new AsyncCallback<Plans[]>() {
				
				@Override
				public void onSuccess(Plans[] result) {
					//Window.alert("grid data recieved");
					if (result.getClass().isArray())
						plansD=(Plans[]) result;
					htmlPanel.clear();
					PlansGridWidget pgdwd=new PlansGridWidget(plansD, htmlPanel);
					
					Widget PlanGridWd=pgdwd.asWidget();
					//htmlPanel.addAndReplaceElement(pdWidget,"form");
					//FrmProject prjFrm=new FrmProject();
					//Widget prjFrmWd=prjFrm.asWidget();
					//htmlPanel.add(prjFrmWd);
					
					htmlPanel.add(PlanGridWd);
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			};
			
			dbService.getPlansGridData(0,plansGridDataCallBack);
			//clear the area
//			htmlPanel.clear();
//			FrmPlan planFrm=new FrmPlan();
//			Widget planFrmWdg=planFrm.asWidget();
//			htmlPanel.add(planFrmWdg);
			
		}
		
		//about.setUrl("http://automobilein.com/wp-content/uploads/2014/02/Rose-Flower-Photos-Free-Download.jpg");
		else if(e.getSelectedItem().getHTML().equals("Reviews"))
		{
			//Window.alert("In Reviews, isn't it?");;
			//clear the area
//			htmlPanel.clear();
//			FrmReview reviewFrm=new FrmReview(1,5);
//			Widget reviewFrmWdg=reviewFrm.asWidget();
//			htmlPanel.add(reviewFrmWdg);
			
			AsyncCallback<Reviews[]> reviewDataCallBack=new AsyncCallback<Reviews[]>() {
				
				@Override
				public void onSuccess(Reviews[] result) {
					htmlPanel.clear();
					ReviewGridWidget rvwGrid=new ReviewGridWidget(result,htmlPanel);
					Widget rvwGrdiWdg=rvwGrid.asWidget();
					htmlPanel.add(rvwGrdiWdg);
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}
			};
			
			dbService.getReviewsByPlan(0,reviewDataCallBack);
		}
		
	}
	
	

	public void setText(String text) {
		
	}

	public String getText() {
		return "Helllo";
	}


	@Override
	public void onFailure(Throwable caught) {
		String pk="kausik";
		result.setText("Error occured try again.....");
		
	}


	@Override
	public void onSuccess(Object result) {
		//this.result.setText("Success");
		if (result.getClass().isArray())
			projs=(Projects[]) result;
		
		Project_details pd=new Project_details(projs,htmlPanel);
		
		Widget pdWidget=pd.asWidget();
		//htmlPanel.addAndReplaceElement(pdWidget,"form");
		//FrmProject prjFrm=new FrmProject();
		//Widget prjFrmWd=prjFrm.asWidget();
		//htmlPanel.add(prjFrmWd);
		
		htmlPanel.add(pdWidget);
		//this.result.setText("result will come here");
		//this.htmlPanel.add(result);
		
		
	}


	@Override
	public void getName(AsyncCallback callback) {
		this.dbService.getName(this);
		
	}


	@Override
	public void getProjectList(int pid,AsyncCallback callback) {
		this.dbService.getProjectList(0,this);
		
	}


	
	public void saveProjectDetails(Projects pProj,
			AsyncCallback<Boolean> callback) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void getProjComboList(AsyncCallback<String[]> callback) {
		// TODO Auto-generated method stub
		
	}



//	@Override
//	public void savePlanDetails(Plans pPlans, AsyncCallback<Boolean> callback) {
//		// TODO Auto-generated method stub
//		
//	}



	@Override
	public void getPlansGridData(int pPid,AsyncCallback<Plans[]> callback) {
		// TODO Auto-generated method stub
		
	}



	



	@Override
	public void saveProjectDetails(Projects pProj, Boolean editMode,
			AsyncCallback<Boolean> callback) {
		// TODO Auto-generated method stub
		
	}



	



	public void getReviews(int pReviewID, AsyncCallback<Reviews> callback) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void saveReviewDetails(Reviews pRev, AsyncCallback<Boolean> callback) {
		// TODO Auto-generated method stub
		
	}



	
//	public void getProjectList(int id, AsyncCallback<Projects[]> callback) {
//		// TODO Auto-generated method stub
//		
//	}



	@Override
	public void getReviewsByPlan(int pPlanId, AsyncCallback<Reviews[]> callback) {
		// TODO Auto-generated method stub
		
	}


//
//	@Override
//	public void getProjectList(int id, AsyncCallback<Projects[]> callback) {
//		// TODO Auto-generated method stub
//		
//	}



	@Override
	public void savePlanDetails(Plans pPlans, Boolean editMode,
			AsyncCallback<Boolean> callback) {
		// TODO Auto-generated method stub
		
	}



//	@Override
//	public void getProjectList(int id, AsyncCallback<Projects[]> callback) {
//		// TODO Auto-generated method stub
//		
//	}
//
//
//
//	@Override
//	public void getReviews(int pReviewID,
//			AsyncCallback<Reviews[]> reviewDataCallBack) {
//		// TODO Auto-generated method stub
//		
//	}

}
