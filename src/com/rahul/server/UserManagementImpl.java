package com.rahul.server;

import java.sql.Connection;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.rahul.client.service.UserManagement;
import com.rahul.model.MUser;
import com.rahul.server.dbqueries.Mqueries;
import com.rahul.server.utils.MySQLAccess;

public class UserManagementImpl extends RemoteServiceServlet implements UserManagement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection dbCon;
	@Override
	public boolean addUser(MUser mUser) {
		boolean retVal = false;
		
		dbCon = MySQLAccess.getConnection();
		try 
		{
			System.out.println("Trying to insert data...");
			String pQuery = "INSERT INTO ac_user (user_id,user_name,user_pwd) VALUES ('" + mUser.getUser_id() +"','" + mUser.getUser_name() + "','" + mUser.getUser_pwd() + "')";
			System.out.println(pQuery);
			retVal = Mqueries.insData(dbCon, pQuery);
			System.out.println("I was able to insert data...");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return retVal;
	}

}
