package com.rahul.model;
import java.io.Serializable;
import java.sql.*;

public class Projects  implements Serializable{
	
	
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Projects()
	{
		
	}
public int getProject_id() {
		return project_id;
	}
	public String getProject_name() {
		return project_name;
	}
	public int getProj_value() {
		return proj_value;
	}
	public int getProj_cost() {
		return proj_cost;
	}
	public int getProj_duration() {
		return proj_duration;
	}
	public Date getProj_start_date() {
		return proj_start_date;
	}
	public Date getProj_end_date() {
		return proj_end_date;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public void setProj_value(int proj_value) {
		this.proj_value = proj_value;
	}
	public void setProj_cost(int proj_cost) {
		this.proj_cost = proj_cost;
	}
	public void setProj_duration(int proj_duration) {
		this.proj_duration = proj_duration;
	}
	public void setProj_start_date(Date proj_start_date) {
		this.proj_start_date = proj_start_date;
	}
	public void setProj_end_date(Date proj_end_date) {
		this.proj_end_date = proj_end_date;
	}
   private int project_id;
   private String project_name;
   private int proj_value;
   private int proj_cost;
   private int proj_duration;
   private Date proj_start_date;
   private Date proj_end_date;
   
   
	

}
