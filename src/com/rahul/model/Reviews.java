package com.rahul.model;
import java.io.Serializable;
import java.sql.Date;

public class Reviews  implements Serializable{
	
	
   /**
	 * 
	 */
		private static final long serialVersionUID = 1L;
		private int mode; //The tag to hold whether in Add/Edit Mode...Liked to have an enum here, but then again...I don't know Java, it Sucks!!!
		private int review_id;
		public void setReview_id(int review_id) {
			this.review_id = review_id;
		}

		private int plan_id;
		private Date start_date;
		private int outsourcing_cost;
		private int bought_out_cost;
		private int direct_charge;
		private int other_cost;
		private int no_of_resources;
		private int duration;
		private Date end_date;  //Not Required. Should be a computed field...Rahul
		private int item_ok;
		private int item_not_ok;
		private int item_missing;
		private Date cdate;
		
	public Reviews()
	{
		mode = 0;
	}

	public Reviews(int pReview_id)
	{
		mode = 1;
		review_id = pReview_id;
	}
	public int getMode()
	{
		return mode;
	}
	public int getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public int getOutsourcing_cost() {
		return outsourcing_cost;
	}

	public void setOutsourcing_cost(int outsourcing_cost) {
		this.outsourcing_cost = outsourcing_cost;
	}

	public int getBought_out_cost() {
		return bought_out_cost;
	}

	public void setBought_out_cost(int bought_out_cost) {
		this.bought_out_cost = bought_out_cost;
	}

	public int getDirect_charge() {
		return direct_charge;
	}

	public void setDirect_charge(int direct_charge) {
		this.direct_charge = direct_charge;
	}

	public int getOther_cost() {
		return other_cost;
	}

	public void setOther_cost(int other_cost) {
		this.other_cost = other_cost;
	}

	public int getNo_of_resources() {
		return no_of_resources;
	}

	public void setNo_of_resources(int no_of_resources) {
		this.no_of_resources = no_of_resources;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public int getItem_ok() {
		return item_ok;
	}

	public void setItem_ok(int item_ok) {
		this.item_ok = item_ok;
	}

	public int getItem_not_ok() {
		return item_not_ok;
	}

	public void setItem_not_ok(int item_not_ok) {
		this.item_not_ok = item_not_ok;
	}

	public int getItem_missing() {
		return item_missing;
	}

	public void setItem_missing(int item_missing) {
		this.item_missing = item_missing;
	}

	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	public int getReview_id() {
		return review_id;
	}

	/*
	public void setReview_id(int review_id) {
		this.review_id = review_id;
	}
	*/
}