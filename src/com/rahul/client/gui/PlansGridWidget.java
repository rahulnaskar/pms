package com.rahul.client.gui;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;
import com.rahul.client.service.DbCommService;
import com.rahul.client.service.DbCommServiceAsync;
import com.rahul.model.Plans;
import com.rahul.model.Projects;
import com.rahul.model.Reviews;

public class PlansGridWidget extends Composite implements HasText {

	private static PlansGridWidgetUiBinder uiBinder = GWT
			.create(PlansGridWidgetUiBinder.class);
	private Plans[] gridData=null;
	private HTMLPanel mainPanelWidget;
	private  DbCommServiceAsync dbService;
	String url;
	

	interface PlansGridWidgetUiBinder extends UiBinder<Widget, PlansGridWidget> {
	}

	public PlansGridWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		
		
	}
	
	public PlansGridWidget(Plans[] pPlanGridData,HTMLPanel pPanel) {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		gridData=pPlanGridData;
		mainPanelWidget=pPanel;
		populatePlansGrid(gridData);
	}

	@UiField
	CellTable<Plans> cellTable;
	@UiField Button bttnAdd;
	@UiHandler("bttnAdd")
	void onClick(ClickEvent e)
	{
		mainPanelWidget.clear();
		FrmPlan planFrm=new FrmPlan();
		Widget planFrmWdg=planFrm.asWidget();
		mainPanelWidget.add(planFrmWdg);
	}
	
	private void populatePlansGrid(Plans[] pData)
	{
		 final SingleSelectionModel<Plans> selectionModel
	     = new SingleSelectionModel<Plans>();

	   cellTable.setSelectionModel(selectionModel);
	   cellTable.addDomHandler(new DoubleClickHandler() {

	        @Override
	        public void onDoubleClick(final DoubleClickEvent event) {
	           final Plans selected = selectionModel.getSelectedObject();
	             
	            if (selected != null) {
	            	//clear the main panel
	            	mainPanelWidget.clear();
	            	FrmPlan planForm=new FrmPlan(selected,true);
	            	Widget planFormWdg=planForm.asWidget();
	            	mainPanelWidget.add(planFormWdg);
	            	
	            	AsyncCallback<Reviews[]> reviewCallBack=new AsyncCallback<Reviews[]>() {
						
						@Override
						public void onSuccess(Reviews[] result) {
							ReviewGridWidget rvwGrid=new ReviewGridWidget(result,mainPanelWidget,selected);
			            	Widget rvwGridWdg=rvwGrid.asWidget();
			            	mainPanelWidget.add(rvwGridWdg);
							
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}
					};
					dbService.getReviewsByPlan(selected.getPlan_id(),reviewCallBack);
	            	
	            	
	            	//Widget prjFrmWidget=prjFrm.asWidget();
	            	
	            	//mainPanel.add(prjFrmWidget);
	            	//getPlanGridDataByProjId(selected.getProject_id());
	            	
	            	

	                    }

	        }

			
	    }, 
	    DoubleClickEvent.getType());
		
		this.cellTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

	   
	    
	    NumberCell Planidnum=new NumberCell();
	    Column<Plans,Number> planIdColumn=new Column<Plans,Number>(Planidnum) {

			@Override
			public Number getValue(Plans object) {
				// TODO Auto-generated method stub
				return object.getPlan_id();
			}
	    	
		};
		cellTable.addColumn(planIdColumn,"Plan ID");
		
		 NumberCell projIdCell=new NumberCell();
		    Column<Plans,Number> projIdColumn=new Column<Plans,Number>(projIdCell) {

				@Override
				public Number getValue(Plans object) {
					// TODO Auto-generated method stub
					return object.getProject_id();
				}
		    	
			};
			cellTable.addColumn(projIdColumn,"Project ID");
			

			 NumberCell outsCostCell=new NumberCell();
			    Column<Plans,Number> projoutsCostColumn=new Column<Plans,Number>(outsCostCell) {

					@Override
					public Number getValue(Plans object) {
						// TODO Auto-generated method stub
						return object.getOutsourcing_cost();
					}
			    	
				};
				cellTable.addColumn(projoutsCostColumn,"Outsourcing Cost");
				
				 NumberCell boutCell=new NumberCell();
				    Column<Plans,Number> projboutColumn=new Column<Plans,Number>(boutCell) {

						@Override
						public Number getValue(Plans object) {
							// TODO Auto-generated method stub
							return object.getBought_out_items();
						}
				    	
					};
					cellTable.addColumn(projboutColumn,"Bought out Cost");
		
		
	    
	    

	    // Add a date column to show the birthday.
	    DateCell StartdateCell = new DateCell();
	    Column<Plans, Date> StartdateColumn = new Column<Plans, Date>(StartdateCell) {
	      @Override
	      public Date getValue(Plans object) {
	        return object.getStart_date();
	      }
	    };
	    cellTable.addColumn(StartdateColumn, "Start Date");
	    
	    DateCell EndDateCell = new DateCell();
	    Column<Plans, Date> EndDateColumn = new Column<Plans, Date>(EndDateCell) {
	      @Override
	      public Date getValue(Plans object) {
	        return object.getEnd_date();
	      }
	    };
	    cellTable.addColumn(EndDateColumn, "End Date");

	    
	    cellTable.setRowCount(1, true);

	    // Push the data into the widget.
	    List<Plans> PROJECTS=Arrays.asList(pData);
	    cellTable.setRowData(0,PROJECTS);

	    // Add it to the root panel.
	   // RootPanel.get().add(table);

	}
	
	
	public PlansGridWidget(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		//button.setText(firstName);
	}

	

	public void setText(String text) {
		//button.setText(text);
	}

	public String getText() {
		return "button";
	}

}
