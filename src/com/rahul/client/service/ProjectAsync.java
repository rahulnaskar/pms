package com.rahul.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.rahul.model.MProject;

public interface ProjectAsync {
	void getProjects(AsyncCallback callback);
}
