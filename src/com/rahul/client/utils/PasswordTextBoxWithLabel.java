package com.rahul.client.utils;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

public class PasswordTextBoxWithLabel extends PasswordTextBox {
	 
	String label;
	boolean isClicked = false;
 
	public PasswordTextBoxWithLabel(String lb) {
		this.label = lb;
		superSetText(label);
		//superSetStyleName("TextBoxWithLabel");
 
		this.addFocusHandler(new FocusHandler() {
 
			@Override
			public void onFocus(FocusEvent event) {
				if (!isClicked) {
					isClicked = true;
					isClicked = true;
					superSetText("");
					//superSetStyleName("TextBoxWithLabelFilled");
				}
			}
		});
 
		this.addBlurHandler(new BlurHandler() {
 
			@Override
			public void onBlur(BlurEvent event) {
				if (isClicked) {
					if (((TextBox) event.getSource()).getText().trim().length() < 1) {
						superSetText(label);
						//superSetStyleName("TextBoxWithLabel");
					}
				}
			}
		});
 
	}
 
	// Nasty Hack
	protected void superSetStyleName(String string) {
		super.setStyleName("TextBoxWithLabel");
	}
 
	protected void superSetText(String text) {
		super.setText(text);
	}
 
	@Override
	public String getText() {
		return super.getText().equals(label) ? "" : super.getText();
	}
 
	@Override
	public void setText(String text) {
		isClicked = true;
		//this.setStyleName("TextBoxWithLabelFilled");
		super.setText(text);
	}
 
}