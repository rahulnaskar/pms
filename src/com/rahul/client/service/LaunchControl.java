package com.rahul.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("loginservice")
public interface LaunchControl extends RemoteService {
	boolean doLogin(String username, String password);
	int addTwoNumber(int num1, int num2);
	String getUserName();
	String getUserID();
}