package com.rahul.model;

import java.io.Serializable;

import com.google.gwt.i18n.shared.DateTimeFormat;

public class MGroup implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long group_id;
	private String group_desc;
	private String own_user_id;
	private String state;
	private DateTimeFormat c_dttm;
	private String u;
	private String g;
	private String o;
	
	public MGroup()
	{
		
	}
	public MGroup(String pGroup_desc)
	{
		group_desc = pGroup_desc;
	}
	public Long getGroup_id() {
		return group_id;
	}
	public void setGroup_id(Long group_id) {
		this.group_id = group_id;
	}
	public String getGroup_desc() {
		return group_desc;
	}
	public void setGroup_desc(String group_desc) {
		this.group_desc = group_desc;
	}
	public String getOwn_user_id() {
		return own_user_id;
	}
	public void setOwn_user_id(String own_user_id) {
		this.own_user_id = own_user_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public DateTimeFormat getC_dttm() {
		return c_dttm;
	}
	public void setC_dttm(DateTimeFormat c_dttm) {
		this.c_dttm = c_dttm;
	}
	public String getU() {
		return u;
	}
	public void setU(String u) {
		this.u = u;
	}
	public String getG() {
		return g;
	}
	public void setG(String g) {
		this.g = g;
	}
	public String getO() {
		return o;
	}
	public void setO(String o) {
		this.o = o;
	}

}
