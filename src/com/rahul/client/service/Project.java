package com.rahul.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.rahul.model.MProject;

@RemoteServiceRelativePath("projectservice")
public interface Project extends RemoteService 
{
	public MProject[] getProjects();
}
