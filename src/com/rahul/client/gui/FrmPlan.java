package com.rahul.client.gui;

import java.sql.Date;

import javax.swing.text.StyledEditorKit.BoldAction;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.rahul.client.service.DbCommService;
import com.rahul.client.service.DbCommServiceAsync;
import com.rahul.model.Plans;

public class FrmPlan extends Composite implements HasText {

	private static FrmPlanUiBinder uiBinder = GWT.create(FrmPlanUiBinder.class);
	private  DbCommServiceAsync dbService=null;
	String url;
	Plans plansData=new Plans();
	Boolean editMode=false;
	

	interface FrmPlanUiBinder extends UiBinder<Widget, FrmPlan> {
	}

	public FrmPlan() {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		dpProj_start.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
		
		//populating list
		AsyncCallback<String[] > prjListCallBack=new AsyncCallback<String[]>() {
			
			@Override
			public void onSuccess(String[] result) {
				proj_list.clear();
				proj_list.addItem("--select--");
				for (int i=0;i<result.length;i++)
				{
					
					proj_list.addItem(result[i]);
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		};
		dbService.getProjComboList(prjListCallBack);
		//populating phase list 
		initPhaseList(proj_phase_list);
		
	}
	
	public FrmPlan(Plans pPlan,Boolean pEditMode) {
		initWidget(uiBinder.createAndBindUi(this));
		url=GWT.getModuleBaseURL()+"databaseService";
		dbService=GWT.create(DbCommService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) dbService;
		endpoint.setServiceEntryPoint(url);
		plansData=pPlan;
		editMode=pEditMode;
		dpProj_start.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
		
		//populating list
		AsyncCallback<String[] > prjListCallBack=new AsyncCallback<String[]>() {
			
			@Override
			public void onSuccess(String[] result) {
				proj_list.clear();
				proj_list.addItem("--select--");
				for (int i=0;i<result.length;i++)
				{
					
					proj_list.addItem(result[i]);
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		};
		dbService.getProjComboList(prjListCallBack);
		//populating phase list 
		initPhaseList(proj_phase_list);
		
		if(editMode)
		{
			initPlanFrom(plansData);
			
		}
		
	}
	
private void initPlanFrom(Plans pPlans)	
{
	 txtproj_bout_item.setValue(Integer.toString(pPlans.getBought_out_items()));
	 txtproj_cust_sup_item.setValue(Integer.toString(pPlans.getCustomer_suppled_item()));
	 txtproj_days.setValue(Integer.toString(pPlans.getDays()));
	 txtproj_other_cost.setValue(Integer.toString(pPlans.getAdditional_cost()));
	 txtproj_outs_cost.setValue(Integer.toString(pPlans.getOutsourcing_cost()));
	 txtproj_resources.setValue(Integer.toString(pPlans.getNo_of_resources()));
	 
	
}
	
@UiField ListBox proj_list;
@UiField ListBox proj_phase_list;
@UiField TextBox txtproj_bout_item;
@UiField TextBox txtproj_outs_cost;
@UiField TextBox txtproj_other_cost;
@UiField TextBox txtproj_resources;
@UiField TextBox txtproj_days;
@UiField DateBox dpProj_start;
@UiField TextBox txtproj_cust_sup_item;


@UiField Button bttnSave;
@UiHandler("bttnSave")
void onClick(ClickEvent e)
{
	AsyncCallback<Boolean> savePlansCallBack=new AsyncCallback<Boolean>() {
		
		@Override
		public void onSuccess(Boolean result) {
			Window.alert("Plans data saved");
			
		}
		
		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			
		}
	};
	dbService.savePlanDetails(initPlanObj(),editMode,savePlansCallBack);
	
}

@SuppressWarnings("deprecation")
private Plans initPlanObj()
{   if(editMode==false)
	{
	plansData=new Plans();
	
   }
	
	
	plansData.setPhase(proj_phase_list.getItemText(proj_phase_list.getSelectedIndex()));
	plansData.setProject_name(proj_list.getItemText(proj_list.getSelectedIndex()));
	plansData.setBought_out_items(Integer.parseInt(txtproj_bout_item.getValue()));
	plansData.setOutsourcing_cost(Integer.parseInt(txtproj_outs_cost.getValue()));
	plansData.setAdditional_cost(Integer.parseInt(txtproj_other_cost.getValue()));
	plansData.setNo_of_resources(Integer.parseInt(txtproj_resources.getValue()));
	plansData.setDays(Integer.parseInt(txtproj_days.getValue()));
	Date start_date=new Date(dpProj_start.getValue().getYear(), dpProj_start.getValue().getMonth(), dpProj_start.getValue().getDate());
	plansData.setStart_date(start_date);
	plansData.setCustomer_suppled_item(Integer.parseInt(txtproj_cust_sup_item.getValue()));
	return plansData; 
	
}

	private void initPhaseList(ListBox pPhaseList)
	{
		pPhaseList.clear();
		pPhaseList.addItem("--Select--");
		pPhaseList.addItem("Initiation");
		pPhaseList.addItem("Planning/Design");
		pPhaseList.addItem("Procurement");
		pPhaseList.addItem("Outsourcing");
		pPhaseList.addItem("Rollout/Delivary");
		pPhaseList.addItem("UAT");
		pPhaseList.addItem("Billing");
		
		
	}
	public void setText(String text) {
		
	}

	public String getText() {
		return "";
	}

}
