package com.rahul.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import com.rahul.client.service.LaunchControl;
import com.rahul.client.service.LaunchControlAsync;

public class Login extends Composite implements AsyncCallback, LaunchControlAsync {

	private LaunchControlAsync service;
	String url;
	AsyncCallback callBack;
	
	private static LoginUiBinder uiBinder = GWT.create(LoginUiBinder.class);

	interface LoginUiBinder extends UiBinder<Widget, Login> {
	}

	public Login() {
		initWidget(uiBinder.createAndBindUi(this));
		url = GWT.getModuleBaseURL() + "loginservice";
		
		this.service = GWT.create(LaunchControl.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);
		
		callBack = this;
	}


	@UiField VerticalPanel vPanel;
	@UiField VerticalPanel mainPanel;
	@UiField VerticalPanel resultPanel;
	@UiField TextBox txtName;
	@UiField PasswordTextBox txtPassword;
	@UiField Button cmdSave;
	@UiField Label lblResult;
	@UiField Label lblName;
	@UiField Label lblPassword;
	@UiField Anchor lnkNew;	

	public Login(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		url = GWT.getModuleBaseURL() + "loginservice";
		
		this.service = GWT.create(LaunchControl.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);
	}

	@UiHandler("cmdSave")
	void onClick(ClickEvent e) {
		doLogin(this.txtName.getText(), this.txtPassword.getText(), Login.this);
	}
	@UiHandler("lnkNew")
	void onSelectMeAnchorClick(ClickEvent event)
	{
		AddUserWidget addUser = new AddUserWidget(this);
		addUser.setModal(true);
		addUser.show();
	}
	
	public void updateLabel(String greeting)
	{
		this.lblResult.setText(greeting);
	}

	public void setCallBackHandler(AsyncCallback callBack)
	{
		this.callBack = callBack;
	}
	
	@Override
	public void onFailure(Throwable caught) {
		this.resultPanel.setVisible(true);
		this.lblResult.setText("Some error has occurred");
	}
	@Override
	public void onSuccess(Object result) {
		//this.resultPanel.setVisible(true);
		
		if (result instanceof Boolean)
		{
			if ((Boolean) result == true)
			{
				
				this.setVisible(false);
				RootLayoutPanel pRootLayoutPanel=RootLayoutPanel.get();
				pRootLayoutPanel.add(new pageAfterLogin());
				RootPanel.get("page_id").add(pRootLayoutPanel);
				

				//this.lblResult.setText("You have logged in successfully");
				this.mainPanel.setVisible(false);
				System.out.println("Server call returned true");
				getUserName(Login.this);
			}
			else
			{
				//System.out.println("The result is not true");
				this.lblResult.setText("Oops...Your credentials didn't match!!!");
			}
		}
		else if (result instanceof String)
		{
			//System.out.println("The result is of string type");
			String tmp = (String) result;
			//this.resultPanel.setVisible(true);
			//this.lblResult.setText("Welcome " + tmp + "!");
			this.setVisible(false);
			pageAfterLogin panelAfterLogin=new pageAfterLogin();
			panelAfterLogin.setVisible(true);
			
		}
		if (callBack != null)
		{
			//this.vPanel.setVisible(false);
			callBack.onSuccess(result);
		}
	}
	@Override
	public void doLogin(String name, String password, AsyncCallback callback) {
		this.service.doLogin(name, password, callback);
		
	}
	@Override
	public void addTwoNumber(int num1, int num2, AsyncCallback callback) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void getUserName(AsyncCallback callback) {
		this.service.getUserName(this);
	}
	@Override
	public void getUserID(AsyncCallback callback) {
		this.getUserName(this);
	}
}
