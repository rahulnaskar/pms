package com.rahul.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.rahul.model.Plans;
import com.rahul.model.Projects;
import com.rahul.model.Reviews;

public interface DbCommServiceAsync {

	void getName(AsyncCallback callback);

	void getProjectList(int id, AsyncCallback<Projects[]> callback);

	void saveProjectDetails(Projects pProj, Boolean editMode,
			AsyncCallback<Boolean> callback);

    void getProjComboList(AsyncCallback<String[]> callback);

	void savePlanDetails(Plans pPlans, Boolean editMode,
			AsyncCallback<Boolean> callback);

	void getPlansGridData(int pProjId, AsyncCallback<Plans[]> callback);
	void getReviews(int pReviewID, AsyncCallback<Reviews> reviewDataCallBack);
	void saveReviewDetails(Reviews pRev, AsyncCallback<Boolean> callback);

	void getReviewsByPlan(int pPlanId, AsyncCallback<Reviews[]> callback);

}
