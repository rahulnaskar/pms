package com.rahul.client.gui;

import java.security.Provider;
import java.util.Arrays;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.rahul.model.MGroup;



public class GroupWidget extends Composite implements HasText {
	
	@SuppressWarnings("deprecation")
	private static final List<MGroup> Groups = Arrays.asList(
	    new MGroup("John"), 
	    new MGroup("Joe"), 
	    new MGroup("Tom"), 
	    new MGroup("Jack"), 
	    new MGroup("Tim"), 
	    new MGroup("Mike"), 
	    new MGroup("George"));
	
	private static GroupWidgetUiBinder uiBinder = GWT
			.create(GroupWidgetUiBinder.class);

	interface GroupWidgetUiBinder extends UiBinder<Widget, GroupWidget> {
	}

	public GroupWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		init();
	}

	//@UiField
	Button cmdSave;
	SimplePager u_pager;
	@SuppressWarnings("rawtypes")
	CellTable u_cellTable;
	@UiField
	HTMLPanel u_mainPanel;

	public GroupWidget(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		init();
		cmdSave.setText(firstName);
	}

	private void init()
	{
		u_cellTable = new CellTable<MGroup>();
		u_cellTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		u_cellTable.setPageSize(4);
		
		u_pager = new SimplePager();
		u_pager.setDisplay(u_cellTable);
		
		
	    // Add a text column to show the name.
	    TextColumn<MGroup> nameColumn = new TextColumn<MGroup>() {
	      @Override
	      public String getValue(MGroup object) {
	        return object.getGroup_desc();
	      }
	    };
	    u_cellTable.addColumn(nameColumn, "Name");
	    
	    u_cellTable.setRowCount(Groups.size(),true);
	    u_cellTable.setRowData(0,Groups);
	    u_mainPanel.add(u_cellTable);
	    
	    
	    u_mainPanel.add(u_pager);
	    u_cellTable.redraw();
	}
	
	
	
    ListDataProvider<MGroup> provider = new ListDataProvider<MGroup>() {
        @Override
        protected void onRangeChanged(HasData<MGroup> display) {
          int start = display.getVisibleRange().getStart();
          int end = start + display.getVisibleRange().getLength();
          end = end >= Groups.size() ? Groups.size() : end;
          List<MGroup> sub = Groups.subList(start, end);
          updateRowData(start, sub);
        }
      };
      
	//@UiHandler("cmdSave")
	void onClick(ClickEvent e) {
		Window.alert("Hello!");
	}

	public void setText(String text) {
		cmdSave.setText(text);
	}

	public String getText() {
		return cmdSave.getText();
	}

}
