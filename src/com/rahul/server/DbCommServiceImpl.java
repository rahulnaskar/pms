package com.rahul.server;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.rahul.client.service.*;
import com.rahul.model.*;
import com.rahul.server.dbqueries.DataProvider;
import com.rahul.server.utils.MySQLAccess;


public class DbCommServiceImpl extends RemoteServiceServlet implements DbCommService {

	public Projects[] getProjectList(int pId) {
	  Connection dbCon = MySQLAccess.getConnection();
		Projects[] prjList=null;
		DataProvider dp=new DataProvider(dbCon);
		try {
			prjList=dp.getProjectDetails(pId);
			dbCon.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return prjList;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "Hello";
	}

	
	public Boolean saveProjectDetails() {
		// TODO Auto-generated method stub
		return null;
	}


	public Boolean saveProjectDetails(Projects pProj,Boolean editMode) {
		Boolean result=false;
		Connection dbCon = MySQLAccess.getConnection();
		DataProvider dp=new DataProvider(dbCon);
		if(dp.saveProject(pProj,editMode))
		{
			try {
				dbCon.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result=true;
		}
		
		return result;
	}


	public String[] getProjComboList() {
		
		Connection dbCon = MySQLAccess.getConnection();
		DataProvider dp=new DataProvider(dbCon);
		String []result=dp.getListofProjects();
		try {
			dbCon.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return result;
	}

	public boolean savePlanDetails(Plans pPlans,Boolean editMode) {
		Boolean result=false;
		Connection dbCon = MySQLAccess.getConnection();
		DataProvider dp=new DataProvider(dbCon);
		if(dp.savePlans(pPlans, editMode))
		{
			try {
				dbCon.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result=true;
		}
		
		return result;
	}
	
	public boolean saveReviewDetails(Reviews pRev) {
		Boolean result=false;
		Connection dbCon = MySQLAccess.getConnection();
		DataProvider dp=new DataProvider(dbCon);
		if(dp.saveReview(pRev))
		{
			try {
				dbCon.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result=true;
		}
		
		return result;
	}
	public Reviews getReviews(int pReviewID) {
		  Connection dbCon = MySQLAccess.getConnection();
			Reviews r = new Reviews(pReviewID);
			DataProvider dp=new DataProvider(dbCon);
			try {
				r=dp.getReview(pReviewID);
				dbCon.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return r;
	}

	
	public Plans[] getPlansGridData(int pPid) {
		Connection dbCon = MySQLAccess.getConnection();
		Plans[] planList=null;
		DataProvider dp=new DataProvider(dbCon);
		try {
			planList=dp.getPlanData(pPid);
			dbCon.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return planList;
	}

	
	public Reviews[] getReviewsByPlan(int pPlanId) {
		Connection dbCon = MySQLAccess.getConnection();
		Reviews[] ReviewList=null;
		DataProvider dp=new DataProvider(dbCon);
		try {
			ReviewList=dp.getReviewsAgainstPlan(pPlanId);
			dbCon.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ReviewList;
	}

	

	
}